const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');

const app = express();

app.use(express.static('build'));
app.use(
	createProxyMiddleware({
		changeOrigin: true,
		target: 'http://localhost:8000',
		ws: true
	})
);
app.listen(5000);
