import React from 'react';
import { Chip, makeStyles } from '@material-ui/core';
import clsx from 'clsx';
import text from 'assets/text';

const foodTypeColor = (theme, foodType) => {
	const mapping = {
		dairy: 'info',
		neutral: 'warning',
		meat: 'error'
	};
	const color = mapping[foodType] || 'secondary';

	return theme.palette[color].main;
};

const useStyles = ({type}) => makeStyles(theme => ({
	root: {
		backgroundColor: foodTypeColor(theme, type)
	}
}));

export default ({className, type}) => {
	const classes = useStyles({type})();

	return <Chip className={clsx(className, classes.root)} label={text.dish.foodTypes[type]}/>;
};
