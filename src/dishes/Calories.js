import React from 'react';
import { Chip } from '@material-ui/core';
import text from 'assets/text';

export default ({calories, className}) => <Chip className={className} label={`${calories} ${text.dish.calories}`} color='secondary'/>;
