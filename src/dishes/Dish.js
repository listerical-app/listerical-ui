import React from 'react';
import { Avatar, ListItem, ListItemAvatar, ListItemText, makeStyles } from '@material-ui/core';
import Calories from './Calories';
import FoodType from './FoodType';

const useStyles = makeStyles(theme => ({
	root: {
		alignItems: 'flex-start'
	},
	image: {
		height: 65,
		width: 110
	},
	text: {
		textAlign: 'start',
		marginInlineStart: `${theme.spacing(2)}px`
	},
	dishAttribute: {
		marginInlineStart: `${theme.spacing(0.5)}px`,
		height: '24px'
	},
	dishSecondaryText: {
		marginBlockStart: `${theme.spacing(1)}px`
	}
}));

export default ({ name, description, image, calories, foodType }) => {
	const classes = useStyles();

	return (
		<ListItem className={classes.root} key={name}>
			<ListItemAvatar>
				<Avatar className={classes.image} alt={name} src={image} variant='square'/>
			</ListItemAvatar>
			<ListItemText className={classes.text} secondary={description} secondaryTypographyProps={{
				className: classes.dishSecondaryText
			}}>
				{name}
				<Calories className={classes.dishAttribute} calories={calories}/>
				<FoodType className={classes.dishAttribute} type={foodType}/>
			</ListItemText>
		</ListItem>
	);
};
