import React from 'react';
import {
	AppBar,
	Avatar,
	List,
	Container,
	Link,
	Toolbar,
	Typography,
	makeStyles
} from '@material-ui/core';
import NavbarTextItem from './NavbarTextItem';
import text from 'assets/text';
import logo from 'assets/logo.png';

import './Navbar.css';

const useStyles = makeStyles(theme => ({
	navDisplayFlex: {
		display: 'flex',
		justifyContent: 'space-between'
	},
	navbarDisplayFlex: {
		display: 'flex',
		justifyContent: 'space-between'
	},
	logoAvatar: {
		width: theme.spacing(8),
		height: theme.spacing(8)
	}
}));

export default () => {
	const classes = useStyles();

	return (
		<AppBar position='static' color='primary'>
			<Toolbar>
				<Container className={classes.navbarDisplayFlex} maxWidth='lg'>
					<List component='nav' className={classes.navDisplayFlex}>
						<NavbarTextItem link='#' text={text.navbar.items.menus}/>
					</List>
					<Link className='navbar-link' href='#' color='inherit' underline='none'>
						<Typography className='navbar-title' align='left' variant='h6'>
							Listerical
						</Typography>
						<Avatar className={classes.logoAvatar} alt='logo' src={logo}/>
					</Link>
				</Container>
			</Toolbar>
		</AppBar>
	);
};
