import React from 'react';
import { ListItem, ListItemText, Link, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
	linkText: {
		textDecoration: 'none',
		textTransform: 'uppercase',
		color: 'white'
	}
}));

export default props => {
	const classes = useStyles();
	const { link, text } = props;

	return (
		<Link href={link} className={classes.linkText} underline='none'>
			<ListItem button>
				<ListItemText primary={text}/>
			</ListItem>
		</Link>
	);
};
