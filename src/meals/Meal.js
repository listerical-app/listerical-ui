import React from 'react';
import { Divider, List, makeStyles } from '@material-ui/core';
import Dish from 'dishes/Dish';
import MealHeader from './MealHeader';

const useStyles = makeStyles(theme => {
	const { grey } = theme.palette;
	const edge = grey[300];
	const center = grey[400];

	return {
		divider: {
			border: 0,
			height: '1px',
			background: center,
			backgroundImage: `linear-gradient(to right, ${edge}, ${center}, ${edge})`
		},
		meals: {
			marginBottom: theme.spacing(1)
		}
	};
});

export default ({ name, openingHours, dishes }) => {
	const classes = useStyles();

	return (
		<>
			<MealHeader name={name} openingHours={openingHours}/>
			<Divider className={classes.divider} variant='middle'/>
			<List className={classes.meals}>
				{dishes.map(dish => <Dish {...dish} key={dish.name}/>)}
			</List>
		</>
	);
};
