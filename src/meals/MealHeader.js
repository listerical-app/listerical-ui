import React from 'react';
import { Typography } from '@material-ui/core';

export default ({name, openingHours}) => (
	<>
		<Typography variant='h5' component='h2'>
			{name}
		</Typography>
		<Typography color='textSecondary'>
			{openingHours}
		</Typography>
	</>
);
