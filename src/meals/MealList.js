import React from 'react';
import { Card, CardContent, makeStyles } from '@material-ui/core';
import Meal from './Meal';

const useStyles = makeStyles(theme => ({
	root: {
		width: '100%',
		backgroundColor: theme.palette.background.paper,
		alignSelf: 'center',
		borderColor: theme.palette.grey[400],
		borderStyle: 'solid',
		borderWidth: '1px'
	}
}));

export default ({meals}) => {
	const classes = useStyles();
    
	return (
		<Card className={classes.root} variant='outlined'>
			<CardContent>
				{meals.map(meal => <Meal {...meal} key={meal.name}/>)}
			</CardContent>
		</Card>
	);
};
