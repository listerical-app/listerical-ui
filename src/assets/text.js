export default {
	dish: {
		calories: 'קלוריות',
		foodTypes: {
			'dairy': 'חלבי',
			'meat': 'בשרי',
			'neutral': 'פרווה'
		}
	},
	menu: {
		datePicker: {
			label: 'תפריט ליום אחר?'
		},
		displayDate: {
			today: 'היום'
		},
		loadErrorMessage: 'משהו השתבש... נסה שוב מאוחר יותר',
		loadingMessage: 'כבר מגיע...',
		menuHeaderPrefix: 'התפריט ל'
	},
	navbar: {
		items: {
			menus: 'תפריטים'
		}
	}
};
