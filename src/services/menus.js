import { formatISO } from 'date-fns';
import { useAPI } from './api';

export const getMeals = async date => {
	const isoDate = formatISO(date, { representation: 'date' });
	const response = await fetch(`/api/menus/${isoDate}`);
	const responseData = await response.json();

	if (response.status !== 200) {
		throw responseData;
	}

	return responseData.meals;
};

export const useMeals = initialDate => {
	const [{ param: date, result: meals, isLoading, isError }, setDate] = useAPI(
		getMeals,
		initialDate,
		[]
	);

	return [{ date, meals, isLoading, isError }, setDate];
};
