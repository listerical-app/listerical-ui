import { useState, useEffect } from 'react';
import camelCaseKeys from 'camelcase-keys';

export const useAPI = (api, initialParam, initialResult = null, camelCase = true) => {
	const [param, setParam] = useState(initialParam);
	const [isLoading, setIsLoading] = useState(false);
	const [isError, setIsError] = useState(false);
	const [result, setResult] = useState(initialResult);

	useEffect(() => {
		const wrapper = async () => {
			setIsError(false);
			setIsLoading(true);
			try {
				let result = await api(param);
				if (camelCase) {
					result = camelCaseKeys(result, { deep: true });
				}
				setResult(result);
			} catch (err) {
				console.error(`Failed calling ${api.name}: ${JSON.stringify(err, null, 4)}`);
				setIsError(true);
				setResult(null);
			} finally {
				setIsLoading(false);
			}
		};

		wrapper();
	}, [api, param, camelCase]);

	return [{ param, result, isLoading, isError }, setParam];
};
