import React from 'react';
import { Container, ThemeProvider } from '@material-ui/core';
import Navbar from 'navbar/Navbar';
import MenuPage from 'menus/MenuPage';
import Gap from 'components/Gap';
import theme from 'themes';

import './App.css';

export default () => (
	<div className='App'>
		<ThemeProvider theme={theme}>
			<Navbar/>
			<Gap verticalHeight={3}/>
			<Container maxWidth='lg'>
				<MenuPage/>
			</Container>
		</ThemeProvider>
	</div>
);
