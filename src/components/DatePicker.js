import React from 'react';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

export default props => (
	<MuiPickersUtilsProvider utils={DateFnsUtils}>
		<KeyboardDatePicker
			variant='inline'
			format='dd/MM/yyyy'
			margin='normal'
			{...props}
		/>
	</MuiPickersUtilsProvider>
);
