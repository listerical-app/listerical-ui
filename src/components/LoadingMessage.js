import React from 'react';
import Typography from '@material-ui/core/Typography';

export default ({message}) => <Typography variant='h5' color='secondary'>{message}</Typography>;
