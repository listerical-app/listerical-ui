import React from 'react';
import Typography from '@material-ui/core/Typography';

export default ({message}) => <Typography variant='h4' color='error'>{message}</Typography>;
