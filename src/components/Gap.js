import React from 'react';

export default ({ verticalHeight }) => {
	const style = {
		height: `${verticalHeight}vh`
	};

	return <div style={style}/>;
};
