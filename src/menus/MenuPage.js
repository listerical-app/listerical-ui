import React from 'react';
import { Container, Grid, makeStyles } from '@material-ui/core';
import Gap from 'components/Gap';
import DatePicker from 'components/DatePicker';
import ErrorMessage from 'components/ErrorMessage';
import LoadingMessage from 'components/LoadingMessage';
import MealList from 'meals/MealList';
import MenuHeader from './MenuHeader';
import { useMeals } from 'services/menus';
import text from 'assets/text';

const useStyles = makeStyles({
	root: {
		display: 'flex',
		flexDirection: 'column'
	},
	header: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	}
});

export default () => {
	const now = new Date();
	const classes = useStyles();
	const [{ date, meals, isLoading, isError }, setDate] = useMeals(now);

	const handleDateChanged = date => {
		setDate(date);
	};

	let message;

	if (isError) {
		message = <ErrorMessage message={text.menu.loadErrorMessage}/>;
	} else if (isLoading) {
		message = <LoadingMessage message={text.menu.loadingMessage}/>;
	} else {
		message = <MealList meals={meals}/>;
	}

	return (
		<Container className={classes.root}>
			<Grid className={classes.header} container>
				<Grid item xs={2}>
					<DatePicker
						label={text.menu.datePicker.label}
						value={date}
						onChange={handleDateChanged}
					/>
				</Grid>
				<Grid item xs={8}>
					<MenuHeader date={date}/>
				</Grid>
				<Grid item xs={2}/>
			</Grid>
			<Gap verticalHeight={3}/>
			{message}
		</Container>
	);
};
