import React from 'react';
import Typography from '@material-ui/core/Typography';
import { format, isToday } from 'date-fns';
import text from 'assets/text';

export default ({date}) => {
	const displayDate = isToday(date) ? text.menu.displayDate.today : format(date, 'dd/MM');

	return <Typography variant='h2'>{text.menu.menuHeaderPrefix}{displayDate}</Typography>;
};
