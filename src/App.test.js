import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders stay tuned text', () => {
	const { getByText } = render(<App/>);
	const linkElement = getByText(/התפריט להיום/i);
	expect(linkElement).toBeInTheDocument();
});
