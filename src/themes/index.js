import { createMuiTheme } from '@material-ui/core/styles';
import * as colors from '@material-ui/core/colors';


const defaultTheme = createMuiTheme({
	direction: 'rtl',
	palette: {
		primary: colors.brown,
		secondary: colors.teal,
	},
	typography: {
		fontFamily: [
			'-apple-system',
			'BlinkMacSystemFont',
			'Rubik',
			'Roboto',
			'"Segoe UI"',
			'"Helvetica Neue"',
			'Arial',
			'sans-serif',
			'"Apple Color Emoji"',
			'"Segoe UI Emoji"',
			'"Segoe UI Symbol"',
		].join(','),
	},
});

export default defaultTheme;
